﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Models
{
    public class PizzaOrder : AuditableEntity
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }
    }
}

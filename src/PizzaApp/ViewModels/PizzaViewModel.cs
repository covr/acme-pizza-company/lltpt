﻿namespace PizzaApp.ViewModels
{
    public class PizzaViewModel
    {
        public string Name { get; set; }
        public string Ingredients { get; set; }
    }
}

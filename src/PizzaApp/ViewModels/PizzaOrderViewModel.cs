﻿using System.ComponentModel.DataAnnotations;
using System;

namespace PizzaApp.ViewModels
{
    public class PizzaOrderViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Customer name is required"), StringLength(200, MinimumLength = 2, ErrorMessage = "Customer name must be between 2 and 200 characters")]
        public string CustomerName { get; set; }

        [Required]
        public string CustomerPhone { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public PizzaViewModel[] Pizzas { get; set; }
    }
}

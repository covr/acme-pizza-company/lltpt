﻿using AutoMapper;
using DAL;
using DAL.Libs.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PizzaApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly IAccountManager _accountManager;
        private readonly IAuthorizationService _authorizationService;
        private const string GetUserByIdActionName = "GetUserById";
        private const string GetRoleByIdActionName = "GetRoleById";
        private const string ReadyForOven = "READY FOR OVEN";
        private readonly ApplicationDbContext _context;

        public OrderController(IAccountManager accountManager, IAuthorizationService authorizationService, ApplicationDbContext applicationDbContext)
        {
            _accountManager = accountManager;
            _authorizationService = authorizationService;
            _context = applicationDbContext;
        }

        [HttpGet]
        [Produces(typeof(UserViewModel))]
        //[Authorize(Authorization.Policies.ManageAllRolesPolicy)] //@todo enable authorization
        public async Task<IActionResult> GetAll()
        {
            //var now = DateTime.Now;
            //"3/9/2018 8:13:00 AM"

            List<PizzaOrder> pizzaOrders;

            using (var context = this._context)
            {
                pizzaOrders = context.PizzaOrders
                    //.Where(b => b.CreatedDate.Date == now)
                    .Include(b => b.Pizzas)
                    .ToList();
            }

            return Ok(Json(pizzaOrders));
        }

        [HttpGet("bakersready")]        
        [Produces(typeof(UserViewModel))]
        //[Authorize(Authorization.Policies.ManageAllRolesPolicy)] //@todo enable authorization
        public async Task<IActionResult> BakersReady(DateTime date)
        {
            List<PizzaOrder> pizzaOrders;

            using (var context = this._context)
            {
                pizzaOrders = context.PizzaOrders
                    .Where(b => b.Status.Contains("READY FOR OVEN"))
                    .Include(b => b.Pizzas)
                    .ToList();
            }

            return Ok(Json(pizzaOrders));
        }

        [HttpPost]
        //[Authorize(Authorization.Policies.ManageAllRolesPolicy)] //@todo enable authorization
        public async Task<IActionResult> CreateOrder([FromBody] PizzaOrder pizzaOrder)
        {
            pizzaOrder.Date = DateTime.UtcNow;
            pizzaOrder.Status = ReadyForOven;

            if (ModelState.IsValid) //@todo extract method
            {
                using (var context = this._context)
                {
                    context.PizzaOrders.Attach(pizzaOrder);
                    context.SaveChanges();
                }
                return NoContent();
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}/finish")]
        //[Authorize(Authorization.Policies.ManageAllRolesPolicy)] //@todo enable authorization
        public async Task<IActionResult> FinishOrder(int id)
        {
            using (var context = this._context)
            {
                var pizzaOrder = new PizzaOrder { Id = id };
                context.PizzaOrders.Attach(pizzaOrder);
                pizzaOrder.Status = "DELIVERED";
                context.SaveChanges();
            }

            return Ok();
        }

        private async Task<UserViewModel> GetUserViewModelHelper(string userId)
        {
            var userAndRoles = await _accountManager.GetUserAndRolesAsync(userId);
            if (userAndRoles == null)
                return null;

            var userVM = Mapper.Map<UserViewModel>(userAndRoles.Item1);
            userVM.Roles = userAndRoles.Item2;

            return userVM;
        }

        private async Task<RoleViewModel> GetRoleViewModelHelper(string roleName)
        {
            var role = await _accountManager.GetRoleLoadRelatedAsync(roleName);
            if (role != null)
                return Mapper.Map<RoleViewModel>(role);

            return null;
        }

        private void AddErrors(IEnumerable<string> errors)
        {
            foreach (var error in errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }
    }
}

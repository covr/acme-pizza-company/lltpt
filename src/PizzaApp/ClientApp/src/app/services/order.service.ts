import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { AccountEndpoint } from './account-endpoint.service';
import { OrderEndpoint } from './order-endpoint.service';
import { AuthService } from './auth.service';
import { User } from '../models/user.model';
import { Role } from '../models/role.model';
import { Permission, PermissionNames, PermissionValues } from '../models/permission.model';
import { UserEdit } from '../models/user-edit.model';

export type RolesChangedOperation = "add" | "delete" | "modify";
export type RolesChangedEventArg = { roles: Role[] | string[], operation: RolesChangedOperation };

@Injectable()
export class OrderService {

    public static readonly roleAddedOperation: RolesChangedOperation = "add";
    public static readonly roleDeletedOperation: RolesChangedOperation = "delete";
    public static readonly roleModifiedOperation: RolesChangedOperation = "modify";

    private _rolesChanged = new Subject<RolesChangedEventArg>();

    private readonly _ordersUrl: string = "/api/orders";

    private http;

    constructor(private router: Router, http: HttpClient, private orderEndpoint: OrderEndpoint) {
        this.http = HttpClient;
    }

    placeOrder(order) {
        return this.orderEndpoint.getOrderEndpoint<any>(order);
    }

    getBakersReady() {
        return this.orderEndpoint.getBakersReadyEndpoint();
    }

    getAllOrders() {
        return this.orderEndpoint.getAllOrdersEndpoint();
    }

    finishOrder(id) {
        return this.orderEndpoint.getFinishOrderEndpoint(id);
    }
}

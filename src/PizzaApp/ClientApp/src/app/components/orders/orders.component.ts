import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { fadeInOut } from '../../services/animations';

import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { ConfigurationService } from '../../services/configuration.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { BootstrapSelectDirective } from "../../directives/bootstrap-select.directive";
import { AccountService } from '../../services/account.service';
import { Utilities } from '../../services/utilities';
import { Permission } from '../../models/permission.model';
import { OrderService } from '../../services/order.service';

@Component({
    selector: 'orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.css'],
    animations: [fadeInOut]
})
export class OrdersComponent {

    orders;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private orderService: OrderService, public configurations: ConfigurationService) {
    }

    ngOnInit() {
        this.reloadFromServer();
    }

    reloadFromServer() {
        this.orderService.getAllOrders()
            .subscribe(result => {
                this.orders = result['value'];

            },
            error => {
                this.alertService.stopLoadingMessage();
                this.alertService.showStickyMessage("Load Error", `Unable to retrieve user preferences from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }
}

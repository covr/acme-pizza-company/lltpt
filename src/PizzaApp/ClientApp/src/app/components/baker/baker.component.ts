import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { fadeInOut } from '../../services/animations';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { ConfigurationService } from '../../services/configuration.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { BootstrapSelectDirective } from "../../directives/bootstrap-select.directive";
import { AccountService } from '../../services/account.service';
import { Utilities } from '../../services/utilities';
import { Permission } from '../../models/permission.model';
import { OrderService } from '../../services/order.service';

@Component({
    selector: 'baker',
    templateUrl: './baker.component.html',
    styleUrls: ['./baker.component.css'],
    animations: [fadeInOut]
})
export class BakerComponent {

    bakersReady;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private orderService: OrderService, public configurations: ConfigurationService) {
    }

    ngOnInit() {
        this.reloadFromServer();
    }

    reloadFromServer() {
        this.orderService.getBakersReady()
            .subscribe(result => {
                this.bakersReady = result['value'];

            },
            error => {
                this.alertService.stopLoadingMessage();
                this.alertService.showStickyMessage("Load Error", `Unable to retrieve user preferences from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }

    finishOrder(id) {
        this.orderService.finishOrder(id)
            .subscribe(results => {
                this.alertService.showMessage("Order Finished! And burnt crust is normal.", "", MessageSeverity.info);
                this.reloadFromServer();
            },
            error => {
                this.alertService.showStickyMessage("Error", `Unable to finish order.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }
}

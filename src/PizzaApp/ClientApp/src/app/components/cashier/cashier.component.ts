import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { fadeInOut } from '../../services/animations';
import { ConfigurationService } from '../../services/configuration.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AccountService } from '../../services/account.service';
import { OrderService } from '../../services/order.service';
import { Utilities } from '../../services/utilities';

@Component({
    selector: 'cashier',
    templateUrl: './cashier.component.html',
    styleUrls: ['./cashier.component.css'],
    animations: [fadeInOut]
})
export class CashierComponent {
    @ViewChild('f')
    private form;

    pizzas = [
        {
            "name": 'Pepperoni Blaster',
            "ingredients": "100% Pepperoni coverage. No visible bread."
        },
        {
            "name": 'Ham Blaster',
            "ingredients": "Ham with burnt hair. Free if cust can keep it down."
        },
        {
            "name": 'Burrito Blaster',
            "ingredients": "Regular pizza with a burrito on top."
        },
        {
            "name": 'Super Supreme',
            "ingredients": "No dough. Just a pile of meat in the shape of a pizza."
        },
        {
            "name": 'Yeti Pie',
            "ingredients": "Alfredo sauce and Yeti meat. Plus hot sauce."
        },
    ]

    order = this.seedOrder();

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private accountService: AccountService, private orderService: OrderService, public configurations: ConfigurationService) {
    }

    public add() {
        this.order.Pizzas.push(this.pizzas[0]);
    }

     minus(event, index) {
        this.order.Pizzas.splice(index, 1);
    }

    createOrder() {
        console.log(this.order);
        this.orderService.placeOrder(this.order)
            .subscribe(results => {
                this.alertService.showMessage("Order Placed", "", MessageSeverity.info);
                this.resetForm();
            },
            error => {
                this.alertService.showStickyMessage("Error", `Unable to submit order.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }

    seedOrder() {
       return {
            CustomerName: '',
            CustomerPhone: '',
            OrderDate: '',
            Pizzas: [this.pizzas[0]]
        }
    }

    resetForm() {
        this.form.reset();
        this.order = this.seedOrder();
    }
}

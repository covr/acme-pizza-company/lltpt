# **ACME Pizza Company** - Covr

*   Scaffolded with Visual Studio 2017 Community
*   ASP.NET MVC Core
*   EF
*   REST API
*   Angular 5
*   Bootstrap, responsive
*   "just enough" applied, lean architecture

## Good parts

*   SPA app, separated from API
*   S.O.L.I.D. seperation of concerns 
*   Combined roles and claims based authorization

## Bad parts

*   Might be difficult to deploy in the future as library versions change
*   Can only run on Windows server
*   FE and BE are in the same SLN, forcing more work for robust horizontal growth

## Installation

*   Extract contents
*   Open the .sln in VS2017
*   Set "PizzaApp" as the StartUp Project
*   Set Run to "PizzaApp". It should default to IIS Express
*   Run it. Wait for dependencies to download. It might take a few minutes.

## Access

```
   Manager: manager@acmepizzaco.com / diTZon^Ah7
   Cashier: cashier@acmepizzaco.com / PR^omEF9rE
   Baker:  baker@acmepizzaco.com / KLmmaF9r!b
```

## Migrations

* Create: dotnet ef migrations add Initial